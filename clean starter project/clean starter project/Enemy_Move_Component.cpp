#include "Enemy_Move_Component.h"
#include "Mouse_Control_Component.h"


ENEMY_MOVE_COMPONENT::ENEMY_MOVE_COMPONENT() : Component(eComponentTypes::ENEMY_MOVE_COMPONENT)
{
}
ENEMY_MOVE_COMPONENT::~ENEMY_MOVE_COMPONENT()
{
}

void ENEMY_MOVE_COMPONENT::Helper_Enemy_Pos_Init()
{
    my_map = (Map*)GetOwner();
    enemy_vec = my_map->Get_enemy_vec();
    enemy_path = my_map->Get_Enemy_Path();
    enemy_start_pos = my_map->Get_enemy_start_pos();
    enemy_end_pos = my_map->Get_enemy_end_pos();
    enemy_spawn_timer = my_map->Get_enemy_respawn_timer();
    enemy_move_timer = my_map->Get_enemy_move_timer();

}

void ENEMY_MOVE_COMPONENT::Initialize()
{
    Helper_Enemy_Pos_Init();
}
void ENEMY_MOVE_COMPONENT::Enemy_Move_Forward()
{
    Helper_Enemy_Pos_Init();
    int a = 0;
    b2Vec2 enemy_curr_position;
    b2Vec2 enemy_move_position;

    for(auto i: enemy_vec)
    {
        (i);
        if(enemy_vec[a]->now_in == 0 && enemy_vec[a]->move_to == 0)
        {
            enemy_curr_position = enemy_start_pos;
            enemy_move_position = enemy_path[enemy_vec[a]->now_in];
            enemy_vec[a]->transform.position.Set(enemy_move_position.x, enemy_move_position.y, 0);
            enemy_vec[a]->move_to++;
        }
        else
        {
            enemy_curr_position = enemy_path[enemy_vec[a]->now_in];
            enemy_move_position = enemy_path[enemy_vec[a]->move_to];
            if(enemy_vec[a]->is_reach_end)
            {
                please_remove_this_enemy_obj = enemy_vec[a];
            }
            if(enemy_move_position == enemy_path.back())
            {
                enemy_vec[a]->transform.position.Set(enemy_move_position.x, enemy_move_position.y, 0);
                enemy_vec[a]->is_reach_end = true;
            }
            else
            {
                enemy_vec[a]->transform.position.Set(enemy_move_position.x, enemy_move_position.y, 0);
                enemy_vec[a]->now_in++;
                enemy_vec[a]->move_to++;
            }

        }

        a++;
    }
}

void ENEMY_MOVE_COMPONENT::Update(float dt)
{
    enemy_move_timer -= dt;
    if(enemy_move_timer < 0 && game_start)
    {
        Enemy_Move_Forward();
        enemy_move_timer = my_map->Get_enemy_move_timer();
    }
    
    
}
