#pragma once
#include "Component.h"
#include <SDL2/SDL_hints.h>
#include <engine/State.h>
#include <engine/Object.h>
#include "Map.h"


class MAP_UPDATE_COMPONENT : public Component
{
public:

    MAP_UPDATE_COMPONENT();
    ~MAP_UPDATE_COMPONENT();
    virtual void Initialize()       override;
    virtual void Update(float dt)   override;
    void Check_Mouse_Pos();
    void Helper_Convert_Mouse_Pos();
    void Helper_Get_Grid_Vec();
private:
    std::vector<Object*> grid_vector;
    std::vector<Map::GRID_KIND> grid_state;
    b2Vec2 mouse_pos;
    b2Vec3 grid_pos;
    Map* my_map;
};