
#include "engine\State.h"
#include "RegisterComponents.h"
#include "engine/ComponentBuilder.h"
#include "DemoUIButton.h"
#include "Map.h"
#include "Map_Init_Component.h"
#include "Map_Update_Component.h"
#include "Mouse_Control_Component.h"
#include "Enemy_Generator.h"
#include  "Enemy_Move_Component.h"
/**
 * \brief 
 * Register each component builder
 */
void RegisterComponents()
{
    State::AddComponentBuilder( eComponentTypes::DemoUIButton,
                                new ComponentTBuilder<DemoUIButton>());
    State::AddComponentBuilder(eComponentTypes::MAP_INIT_COMPONENT,
        new ComponentTBuilder<MAP_INIT_COMPONENT>());
    State::AddComponentBuilder(eComponentTypes::MAP_UPDATE_COMPONENT,
        new ComponentTBuilder<MAP_UPDATE_COMPONENT>());
    State::AddComponentBuilder(eComponentTypes::MOUSE_CONTROL_COMPONENT,
        new ComponentTBuilder<MOUSE_CONTROL_COMPONENT>());
    State::AddComponentBuilder(eComponentTypes::ENEMY_GENERATOR,
        new ComponentTBuilder<ENEMY_GENERATOR>());
    State::AddComponentBuilder(eComponentTypes::ENEMY_MOVE_COMPONENT,
        new ComponentTBuilder<ENEMY_MOVE_COMPONENT>());
}
void UnregisterComponents()
{
    State::RemoveComponentBuilder(eComponentTypes::DemoUIButton);
}
