#pragma once
#include "engine\State.h"
#include "engine\Object.h"
#include "Map.h"

extern Object* please_add_this;
extern Object* please_add_this_enemy;
extern Object* please_remove_this_enemy_obj;
extern bool please_remove_this;
extern bool please_add_this_enemy_once;
extern bool please_remove_this_enemy;
extern bool game_start;




class Level1 : public State
{
    friend class Game;

protected:
    Level1() : State() {};
    ~Level1() override {};

    void Initialize() override;
    void Update(float dt) override;
    void Close() override;

private:
    Map level1_map;
};
