/******************************************************************************/
/*!
\file   ComponentTypes.h
\author Hun Yang
\par    email: hun.yang8456@gmail.com
\par    GAM150 demo
\date   2019/04/30


*/
/******************************************************************************/
#pragma once

#include <string>

/**
 * \brief 
 * This is enum class for component types
 */
enum class eComponentTypes
{
    INVALID = 0,
    DemoUIButton,
    MAP_INIT_COMPONENT,
    MAP_UPDATE_COMPONENT,
    MOUSE_CONTROL_COMPONENT,
    ENEMY_GENERATOR,
    ENEMY_MOVE_COMPONENT,

	NUM_COMPONENTS
};

/**
 * \brief 
 * Convert string to eComponentTypes's component
 * 
 * \param string
 * String to be  converted for component type
 * 
 * \return 
 * Converted component type
 */
inline eComponentTypes StringToComponent(const std::string& string)
{
    if (string == "DemoUIButton")			        return eComponentTypes::DemoUIButton;
    if (string == "MAP_INIT_COMPONENT")                      return eComponentTypes::MAP_INIT_COMPONENT;
    if (string == "MAP_UPDATE_COMPONENT")                      return eComponentTypes::MAP_UPDATE_COMPONENT;
    if (string == "MOUSE_CONTROL_COMPONENT")                      return eComponentTypes::MOUSE_CONTROL_COMPONENT;
    if (string == "ENEMY_GENERATOR")                      return eComponentTypes::ENEMY_GENERATOR;
    if (string == "ENEMY_MOVE_COMPONENT")                      return eComponentTypes::ENEMY_MOVE_COMPONENT;
	return eComponentTypes::INVALID;
}

/**
 * \brief 
 * Convert eComponentTypes's component to string
 * 
 * \param type 
 * Component type to be converted for string
 * 
 * \return 
 * Converted string
 */
inline std::string ComponentToString(eComponentTypes type)
{
    if (type == eComponentTypes::DemoUIButton)		            return "DemoUIButton";
    if (type == eComponentTypes::MAP_INIT_COMPONENT)                     return "MAP_INIT_COMPONENT";
    if (type == eComponentTypes::MAP_UPDATE_COMPONENT)                     return "MAP_UPDATE_COMPONENT";
    if (type == eComponentTypes::MOUSE_CONTROL_COMPONENT)                     return "MOUSE_CONTROL_COMPONENT";
    if (type == eComponentTypes::ENEMY_GENERATOR)                     return "ENEMY_GENERATOR";
    if (type == eComponentTypes::ENEMY_MOVE_COMPONENT)                     return "ENEMY_MOVE_COMPONENT";
	return "Invalid";
}