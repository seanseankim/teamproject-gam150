#include "Enemy_Generator.h"
#include "Mouse_Control_Component.h"


ENEMY_GENERATOR::ENEMY_GENERATOR() : Component(eComponentTypes::ENEMY_GENERATOR)
{
}
ENEMY_GENERATOR::~ENEMY_GENERATOR()
{

}
void ENEMY_GENERATOR::Helper_Enemy_Pos_Init()
{
    my_map = (Map*)GetOwner();
    enemy_vec = my_map->Get_enemy_vec();
    enemy_path = my_map->Get_Enemy_Path();
    enemy_start_pos = my_map->Get_enemy_start_pos();
    enemy_end_pos = my_map->Get_enemy_end_pos();
    enemy_spawn_timer = my_map->Get_enemy_respawn_timer();

}
void ENEMY_GENERATOR::Helper_Enemy_Vector_Set()
{
   //my_map = (Map*)GetOwner();
   //enemy_vec = my_map->Get_enemy_vec();
   //enemy_path = my_map->Get_Enemy_Path();
   //enemy_start_pos = my_map->Get_enemy_start_pos();
   //enemy_end_pos = my_map->Get_enemy_end_pos();
   //enemy_spawn_timer = my_map->Get_enemy_respawn_timer();

    my_map->set_enemy_vec(enemy_vec);

}
void ENEMY_GENERATOR::Initialize()
{
    Helper_Enemy_Pos_Init();
}


void ENEMY_GENERATOR::Spawn_Enemy()
{
    Helper_Enemy_Pos_Init();
    temp_enemy = new Enemy;
    temp_enemy->SetName("enemy");
    temp_enemy->transform.position.Set(enemy_start_pos.x, enemy_start_pos.y, 0.0f);
    temp_enemy->transform.SetScale(100, 100);
    temp_enemy->sprite.LoadImage("texture/enemy.png", State::m_renderer);
    temp_enemy->priority = enemy_proiorty;
    enemy_proiorty++;
    please_add_this_enemy = temp_enemy;
    please_add_this_enemy_once = true;
    enemy_vec.push_back(temp_enemy);
    Helper_Enemy_Vector_Set();
}
void ENEMY_GENERATOR::Update(float dt)
{
    enemy_spawn_timer -= dt;
    if(enemy_spawn_timer < 0)
    {
        Spawn_Enemy();

        enemy_spawn_timer = my_map->Get_enemy_respawn_timer();
    }

    
}
