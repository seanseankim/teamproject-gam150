#include "Map_Init_Component.h"
#include <iostream>
#include "Mouse_Control_Component.h"
#include <crtdbg.h>

void MOUSE_CONTROL_COMPONENT::Helper_Get_Grid_Vec()
{
    my_map = (Map*)GetOwner();
    grid_state = my_map->Get_Grid_Kind_Vector();
    grid_vector = my_map->Get_Grid_Vector();
    enemy_path = my_map->Get_Enemy_Path();
}

void MOUSE_CONTROL_COMPONENT::Helper_Convert_Mouse_Pos()
{
    mouse_pos = State::m_input->GetMousePos();
    mouse_pos.x -= State::m_width / 2;
    mouse_pos.y -= State::m_height / 2;
    mouse_pos.y *= -1;
}
void MOUSE_CONTROL_COMPONENT::Helper_Set_Grid_Vec()
{
    my_map->Set_Grid_State_Vector(grid_state);
    my_map->Set_Grid_Vector(grid_vector);
    my_map->Set_Enemy_Path(enemy_path);
}

MOUSE_CONTROL_COMPONENT::MOUSE_CONTROL_COMPONENT() : Component(eComponentTypes::MOUSE_CONTROL_COMPONENT), following(nullptr)
{
}
MOUSE_CONTROL_COMPONENT::~MOUSE_CONTROL_COMPONENT()
{
}
void MOUSE_CONTROL_COMPONENT::Initialize()
{
    my_map = (Map*)GetOwner();
    grid_vector = my_map->Get_Grid_Vector();
    grid_state = my_map->Get_Grid_Kind_Vector();
}
void MOUSE_CONTROL_COMPONENT::Check_Mouse_Click()
{
    int a = 0;
    Helper_Convert_Mouse_Pos();
    for (auto i : grid_vector)
    {
        grid_pos = i->transform.position;
        if (mouse_pos.x > grid_pos.x - i->transform.GetScale().x / 2 &&
            mouse_pos.x < grid_pos.x + i->transform.GetScale().x / 2 &&
            mouse_pos.y > grid_pos.y - i->transform.GetScale().y / 2 &&
            mouse_pos.y < grid_pos.y + i->transform.GetScale().y / 2)
        {
            if (grid_state[a] == Map::CHUL_TOWER_BUTTON)
            {
                std::cout << "chul tower button" << std::endl;
                current_mouse_state = CHUL_TOWER_SETTING;
                Generate_Following_Thing(grid_state[a]);
            }
            else if (grid_state[a] == Map::CHUL_TOWER_SETTED)
            {
                std::cout << "chul tower button set" << std::endl;

                Generate_Following_Thing(grid_state[a]);
            }
            else if (grid_state[a] == Map::START_BUTTON)
            {
                std::cout << "start button" << std::endl;
                current_mouse_state = START_BUTTON_SETTING;
                Generate_Following_Thing(grid_state[a]);
            }
            else if (grid_state[a] == Map::END_BUTTON)
            {
                std::cout << "end button" << std::endl;
                current_mouse_state = END_BUTTON_SETTING;
                Generate_Following_Thing(grid_state[a]);
            }
            else if (grid_state[a] == Map::WAY_BUTTON)
            {
                std::cout << "way button" << std::endl;
                current_mouse_state = WAY_SETTING;
                Generate_Following_Thing(grid_state[a]);
            }
            else if (grid_state[a] == Map::START)
            {
                std::cout << "start" << std::endl;
                Generate_Following_Thing(grid_state[a]);
                //current_mouse_state = START;
                game_start = true;
            }
            else if (grid_state[a] == Map::TOWER_POSITION_BUTTON)
            {
                std::cout << "tower position button" << std::endl;
                current_mouse_state = TOWER_POSITION_SETTING;
                Generate_Following_Thing(grid_state[a]);
            }
            else
            {
                std::cout << "nothing" << std::endl;
                current_mouse_state = NOTHING;
            }
        }
        a++;
    }
}
void MOUSE_CONTROL_COMPONENT::Mouse_Setting()
{
    int a = 0;
    Helper_Get_Grid_Vec();
    b2Vec2 temp_vec;

    for (auto i : grid_vector)
    {
        grid_pos = i->transform.position;
        if (mouse_pos.x > grid_pos.x - i->transform.GetScale().x / 2 &&
            mouse_pos.x < grid_pos.x + i->transform.GetScale().x / 2 &&
            mouse_pos.y > grid_pos.y - i->transform.GetScale().y / 2 &&
            mouse_pos.y < grid_pos.y + i->transform.GetScale().y / 2)
        {
            if (current_mouse_state == CHUL_TOWER_SETTING)
            {
                if (grid_state[a] == Map::TOWER_POSITION_BUTTON_SETTED)
                {
                    grid_state[a] = Map::CHUL_TOWER_SETTED;
                    std::cout << "tower setted" << std::endl;
                    i->sprite.color = { 200,200,200,200 };
                    following = nullptr;
                    current_mouse_state = NOTHING;
                    please_remove_this = true;
                }
            }
            else
            {
                if (grid_state[a] == NOTHING)
                {
                    switch (current_mouse_state)
                    {
                    case START_BUTTON_SETTING:
                        grid_state[a] = Map::START_BUTTON_SETTED;
                        std::cout << "start setted" << std::endl;
                        i->sprite.color = { 200,123,123,200 };
                        following = nullptr;
                        current_mouse_state = NOTHING;
                        please_remove_this = true;
                        temp_vec.x = grid_pos.x;
                        temp_vec.y = grid_pos.y;
                        my_map->Set_enemy_start_pos(temp_vec);
                        break;

                    case END_BUTTON_SETTING:

                        grid_state[a] = Map::END_BUTTON_SETTED;
                        std::cout << "end setted" << std::endl;
                        i->sprite.color = { 255,34,23,200 };
                        following = nullptr;
                        current_mouse_state = NOTHING;
                        please_remove_this = true;
                        temp_vec.x = grid_pos.x;
                        temp_vec.y = grid_pos.y;
                        my_map->Set_enemy_end_pos(temp_vec);
                        break;

                    case WAY_SETTING:

                        grid_state[a] = Map::WAY_BUTTON_SETTED;
                        std::cout << "way setted" << std::endl;
                        i->sprite.color = { 12,200,12,200 };
                        temp_vec.x = grid_pos.x;
                        temp_vec.y = grid_pos.y;
                        enemy_path.push_back(temp_vec);

                        break;

                    case TOWER_POSITION_SETTING:

                        grid_state[a] = Map::TOWER_POSITION_BUTTON_SETTED;
                        std::cout << "tower position setted" << std::endl;
                        i->sprite.color = { 1,23,233,200 };
                        following = nullptr;
                        current_mouse_state = NOTHING;
                        please_remove_this = true;
                        break;
                    }
                }
            }
        }

        a++;
    }

    Helper_Set_Grid_Vec();
}

void MOUSE_CONTROL_COMPONENT::Generate_Following_Thing(Map::GRID_KIND grid_kind_curr)
{
    switch (grid_kind_curr)
    {
    case Map::GRID_KIND::CHUL_TOWER_BUTTON:
        following = new Object;
        following->sprite.LoadImage("texture/rect.png", State::m_renderer);
        following->sprite.color = { 50,50,100,100 };
        following->transform.SetScale(80, 80);
        following->transform.rotation = 0;
        please_add_this = following;
        break;

    case Map::GRID_KIND::START_BUTTON:
        following = new Object;
        following->sprite.LoadImage("texture/rect.png", State::m_renderer);
        following->sprite.color = { 50,50,100,100 };
        following->transform.SetScale(80, 80);
        following->transform.rotation = 0;
        please_add_this = following;
        break;

    case Map::GRID_KIND::END_BUTTON:
        following = new Object;
        following->sprite.LoadImage("texture/rect.png", State::m_renderer);
        following->sprite.color = { 50,50,100,100 };
        following->transform.SetScale(80, 80);
        following->transform.rotation = 0;
        please_add_this = following;
        break;

    case Map::GRID_KIND::WAY_BUTTON:
        following = new Object;
        following->sprite.LoadImage("texture/rect.png", State::m_renderer);
        following->sprite.color = { 50,50,100,100 };
        following->transform.SetScale(80, 80);
        following->transform.rotation = 0;
        please_add_this = following;
        break;

    case Map::GRID_KIND::TOWER_POSITION_BUTTON:
        following = new Object;
        following->sprite.LoadImage("texture/rect.png", State::m_renderer);
        following->sprite.color = { 50,50,100,100 };
        following->transform.SetScale(80, 80);
        following->transform.rotation = 0;
        please_add_this = following;

        break;
    }

}
void MOUSE_CONTROL_COMPONENT::Check_Following_State()
{
    int a = 0;
    Helper_Convert_Mouse_Pos();
    for (auto i : grid_vector)
    {
        grid_pos = i->transform.position;
        if (mouse_pos.x > grid_pos.x - i->transform.GetScale().x / 2 &&
            mouse_pos.x < grid_pos.x + i->transform.GetScale().x / 2 &&
            mouse_pos.y > grid_pos.y - i->transform.GetScale().y / 2 &&
            mouse_pos.y < grid_pos.y + i->transform.GetScale().y / 2)
        {
            switch (current_mouse_state)
            {
            case CHUL_TOWER_SETTING:
                following->sprite.color = { 200,0,0,100 };
                if(grid_state[a] == Map::GRID_KIND::TOWER_POSITION_BUTTON_SETTED)
                {
                    following->sprite.color = { 0,200,0,100 };
                }
                break;
            case START_BUTTON_SETTING:
                if (grid_state[a] == Map::GRID_KIND::NOTHING)
                {
                    following->sprite.color = { 0,200,0,100 };
                }
                else if (grid_state[a] != Map::GRID_KIND::NOTHING)
                {
                    following->sprite.color = { 200,0,0,100 };
                }
            case END_BUTTON_SETTING:
                if (grid_state[a] == Map::GRID_KIND::NOTHING)
                {
                    following->sprite.color = { 0,200,0,100 };
                }
                else if (grid_state[a] != Map::GRID_KIND::NOTHING)
                {
                    following->sprite.color = { 200,0,0,100 };
                }
            case WAY_SETTING:
                if (grid_state[a] == Map::GRID_KIND::NOTHING)
                {
                    following->sprite.color = { 0,200,0,100 };
                }
                else if (grid_state[a] != Map::GRID_KIND::NOTHING)
                {
                    following->sprite.color = { 200,0,0,100 };
                }
            case TOWER_POSITION_SETTING:
                if (grid_state[a] == Map::GRID_KIND::NOTHING)
                {
                    following->sprite.color = { 0,200,0,100 };
                }
                else if (grid_state[a] != Map::GRID_KIND::NOTHING)
                {
                    following->sprite.color = { 200,0,0,100 };
                }
            }
        }
        a++;
    }

}
void MOUSE_CONTROL_COMPONENT::Update(float dt)
{
    (dt);
    if (State::m_input->IsTriggered(SDL_BUTTON_LEFT))
    {
        if (current_mouse_state == NOTHING)
        {
            please_remove_this = false;
            Check_Mouse_Click();
        }
        else if (current_mouse_state != NOTHING)
        {
            Mouse_Setting();
        }
    }

    else if (State::m_input->IsTriggered(SDL_BUTTON_RIGHT))
    {
        please_remove_this = true;
        current_mouse_state = NOTHING;
    }
    if (following != nullptr)
    {
        Helper_Convert_Mouse_Pos();
        Check_Following_State();
        following->transform.position.Set(mouse_pos.x, mouse_pos.y, 0.f);
    }
}
