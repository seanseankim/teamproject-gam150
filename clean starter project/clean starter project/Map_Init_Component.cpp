#include "Map_Init_Component.h"


MAP_INIT_COMPONENT::MAP_INIT_COMPONENT(): Component(eComponentTypes::MAP_INIT_COMPONENT)
{
}
MAP_INIT_COMPONENT::~MAP_INIT_COMPONENT()
{
    
}

void MAP_INIT_COMPONENT::Initialize()
{
    Uint8 color_g = 100;
    Uint8 color_r = 100;
    float init_map_x = -State::m_width / 2.0f;
    float init_map_y = State::m_height / 2.0f;
    const float map_cut_24_x = (State::m_width / 16.f);
    const float map_cut_24_y = (State::m_height / 9.f);
    Map* my_map;
    my_map = (Map*)GetOwner();
    std::vector<Object*> asd;
    asd = my_map->Get_Grid_Vector();
    

    for (int i = 0; i < cut_nine; i++)
    {
        for (int j = 0; j < cut_sixteen; j++)
        {
            if (i == 4)
            {
                color_g = 0;
                color_r = 100;
            }
            else
            {
                color_g = 100;
                color_r = 0;

            }
            grid = new Object;
            grid->transform.SetScale(map_cut_24_x, map_cut_24_y);
            grid->SetName("grid");
            grid->transform.position.Set(init_map_x + (map_cut_24_x / 2.f), init_map_y - (map_cut_24_y / 2.f), 1);
            grid->transform.rotation = 0;
            grid->sprite.LoadImage("texture/rect.png", State::m_renderer);
            //grid->sprite.color = { 0,0,0,255 };
            grid_vector.push_back(grid);
            grid_state.push_back(Map::NOTHING);
            
            init_map_x += map_cut_24_x;
        }
        init_map_x = -State::m_width / 2.0f;
        init_map_y -= map_cut_24_y;
    }
    grid_state[15] = Map::CHUL_TOWER_BUTTON;
    grid_state[31] = Map::START_BUTTON;
    grid_state[47] = Map::END_BUTTON;
    grid_state[63] = Map::WAY_BUTTON;
    grid_state[79] = Map::START;
    grid_state[95] = Map::TOWER_POSITION_BUTTON;
    Set_Grid();
    my_map->Get_Grid_Vector() = grid_vector;
    my_map->Set_Grid_State_Vector(grid_state);
}
void MAP_INIT_COMPONENT::Set_Grid()
{
    int a = 0;
    for(auto i: grid_vector)
    {
        if(grid_state[a] == Map::CHUL_TOWER_BUTTON)
        {
            i->sprite.color = { 100,100,100,100 };
        }
        else if (grid_state[a] == Map::CHUL_TOWER_SETTED)
        {
            i->sprite.color = { 100,100,0,100 };
        }
        else if (grid_state[a] == Map::START_BUTTON)
        {
            i->sprite.color = { 0,0,100,100 };
        }
        else if (grid_state[a] == Map::END_BUTTON)
        {
            i->sprite.color = { 100,0,0,100 };
        }
        else if (grid_state[a] == Map::WAY_BUTTON)
        {
            i->sprite.color = { 0,100,100,100 };
        }
        else if (grid_state[a] == Map::START)
        {
            i->sprite.color = { 0,0,0,200};
        }
        else if (grid_state[a] == Map::TOWER_POSITION_BUTTON)
        {
            i->sprite.color = { 50,20,30,200 };
        }
        a++;
    }

}


void MAP_INIT_COMPONENT::Update(float dt)
{
    (dt);
}
