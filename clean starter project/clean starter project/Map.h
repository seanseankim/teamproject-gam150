#pragma once
#include <Box2D/Common/b2Math.h>
#include <vector>
#include <engine/Object.h>
#include "engine\State.h"
#include "Component.h"
#include "Enemy.h"

class Map : public Object
{
public:
    enum GRID_KIND
    {
        NOTHING,
        UNSELECTED,
        SELECTED,
        ENEMY_PATH,
        CHUL_TOWER_BUTTON,
        CHUL_TOWER_SETTED,
        START_BUTTON,
        START_BUTTON_SETTED,
        END_BUTTON,
        END_BUTTON_SETTED,
        WAY_BUTTON,
        WAY_BUTTON_SETTED,
        TOWER_POSITION_BUTTON,
        TOWER_POSITION_BUTTON_SETTED,
        START
    };
    Map() = default;
    ~Map()
    {
        
    }
    void Init_Map();
    void Set_Grid_Vector(std::vector<Object*> grid)
    {
        this->grid_vector = grid;
    }
    void Set_Grid_State_Vector(std::vector<GRID_KIND> grid_state_vec)
    {
        this->grid_state = grid_state_vec;
    }

    std::vector<Object*>& Get_Grid_Vector()
    {
        return grid_vector;
    }
    std::vector<GRID_KIND>& Get_Grid_Kind_Vector()
    {
        return grid_state;
    }
    std::vector<b2Vec2>& Get_Enemy_Path()
    {
        return enemy_path;
    }
    void Set_Enemy_Path(std::vector<b2Vec2> enemy_path_vec)
    {
        enemy_path = enemy_path_vec;
    }
    void Set_enemy_start_pos(b2Vec2 start_pos)
    {
        enemy_start_pos = start_pos;
    }
    void Set_enemy_end_pos(b2Vec2 end_pos)
    {
        enemy_end_pos = end_pos;
    }
    std::vector<b2Vec2>& Get_enemy_path()
    {
        return enemy_path;
    }

    b2Vec2& Get_enemy_start_pos()
    {
        return enemy_start_pos;
    }

    b2Vec2& Get_enemy_end_pos()
    {
        return enemy_end_pos;
    }

    std::vector<Enemy*>& Get_enemy_vec()
    {
        return enemy_vec;
    }

    void set_enemy_vec(std::vector<Enemy*>& enemy_vec_)
    {
        enemy_vec = enemy_vec_;
    }
    float Get_enemy_respawn_timer()
    {
        return enemy_respawn_timer;
    }
    float Get_enemy_move_timer()
    {
        return enemy_move_timer;
    }
private:
    int asdf = 10;
    std::vector<Object*> grid_vector;
    std::vector<GRID_KIND> grid_state;
    std::vector<Enemy*> enemy_vec;
    std::vector<b2Vec2> enemy_path;
    b2Vec2 enemy_start_pos;
    b2Vec2 enemy_end_pos;
    float enemy_respawn_timer = 3.0f;
    float enemy_move_timer = 2.0f;
};
