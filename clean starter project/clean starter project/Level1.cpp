#include "Level1.h"

extern Object* please_add_this = nullptr;
extern bool please_remove_this = false;
extern Object* please_add_this_enemy = nullptr;
extern bool please_remove_this_enemy = false;
extern bool game_start = false;
extern bool please_add_this_enemy_once = false;
extern Object* please_remove_this_enemy_obj = nullptr;


void Level1::Initialize()
{
    camera.position.Set(0, 0, 0);
    AddComponentToObject(&level1_map, eComponentTypes::MAP_INIT_COMPONENT);
    AddComponentToObject(&level1_map, eComponentTypes::MAP_UPDATE_COMPONENT);
    AddComponentToObject(&level1_map, eComponentTypes::MOUSE_CONTROL_COMPONENT);
    AddComponentToObject(&level1_map, eComponentTypes::ENEMY_GENERATOR);
    AddComponentToObject(&level1_map, eComponentTypes::ENEMY_MOVE_COMPONENT);
    
    AddObject(&level1_map);
    InitializeObjects();
    for (auto i : level1_map.Get_Grid_Vector())
    {
        AddObject(i);
    }
}

void Level1::Update(float dt)
{
    if(please_add_this != nullptr)
    {
        AddObject(please_add_this);
    }
    if(please_remove_this)
    {
        RemoveObject(please_add_this);
    }
    if (game_start)
    {
        if (please_add_this_enemy != nullptr && please_add_this_enemy_once)
        {
            AddObject(please_add_this_enemy);
            please_add_this_enemy_once = false;
        }
        if(please_remove_this_enemy_obj != nullptr)
        {
            RemoveObject(please_remove_this_enemy_obj);
            please_remove_this_enemy_obj = nullptr;
        }
    }
    UpdateObjects(dt);
    UpdatePhysics(dt);
    Render(dt);
}

void Level1::Close()
{
    ClearBaseState();
    for(auto i: level1_map.Get_Grid_Vector())
    {
        delete i;
    }
    for(auto i : level1_map.Get_enemy_vec())
    {
        delete i;
    }
}
