#pragma once
#include "Component.h"
#include <SDL2/SDL_hints.h>
#include <engine/State.h>
#include <engine/Object.h>
#include "Map.h"


class MAP_INIT_COMPONENT : public Component
{
public:

    MAP_INIT_COMPONENT();
    ~MAP_INIT_COMPONENT();
    virtual void Initialize()       override;
    virtual void Update(float dt)   override;
    void Set_Grid();
private:
    const int cut_sixteen = 16;
    const int cut_nine = 9;
    Object* grid;
    
    std::vector<Object*> grid_vector;
    std::vector<Map::GRID_KIND> grid_state;
    b2Vec2 mouse_pos;
    b2Vec3 grid_pos;
};