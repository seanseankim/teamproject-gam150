#pragma once
#include <string>

enum class eCommandTypes
{
    INVALID = 0,
    QuitCommand,
    MAP_INIT_COMPONENT,
    Count
};


inline eCommandTypes StringToCommand(const std::string& string)
{
    if (string == "QuitCommand")				    return eCommandTypes::QuitCommand;
    if (string == "MAP_INIT_COMPONENT")             return eCommandTypes::MAP_INIT_COMPONENT;
    return eCommandTypes::INVALID;
}

inline std::string CommandToString(eCommandTypes type)
{
    if (type == eCommandTypes::QuitCommand)				        return "QuitCommand";
    if (type == eCommandTypes::MAP_INIT_COMPONENT)               return "MAP_INIT_COMPONENT";
    return "Invalid";
}