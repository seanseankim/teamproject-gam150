#include "Map_Update_Component.h"


MAP_UPDATE_COMPONENT::MAP_UPDATE_COMPONENT() : Component(eComponentTypes::MAP_UPDATE_COMPONENT)
{
}
MAP_UPDATE_COMPONENT::~MAP_UPDATE_COMPONENT()
{

}
void MAP_UPDATE_COMPONENT::Helper_Get_Grid_Vec()
{
    my_map = (Map*)GetOwner();
    grid_state = my_map->Get_Grid_Kind_Vector();
    grid_vector = my_map->Get_Grid_Vector();
}

void MAP_UPDATE_COMPONENT::Initialize()
{
    Helper_Get_Grid_Vec();
}
void MAP_UPDATE_COMPONENT::Helper_Convert_Mouse_Pos()
{
    mouse_pos = State::m_input->GetMousePos();
    mouse_pos.x -= State::m_width / 2;
    mouse_pos.y -= State::m_height / 2;
    mouse_pos.y *= -1;
}

void MAP_UPDATE_COMPONENT::Check_Mouse_Pos()
{
    Helper_Convert_Mouse_Pos();
    Helper_Get_Grid_Vec();
    int a = 0;

    for(auto i: grid_vector)
    {
        grid_pos = i->transform.position;
        if (mouse_pos.x > grid_pos.x - i->transform.GetScale().x / 2 &&
            mouse_pos.x < grid_pos.x + i->transform.GetScale().x / 2 &&
            mouse_pos.y > grid_pos.y - i->transform.GetScale().y / 2 &&
            mouse_pos.y < grid_pos.y + i->transform.GetScale().y / 2)
        {
            i->sprite.color = { 0,100,0,100 };
            i->SetName("asd");
        }
        else if (grid_state[a] == Map::CHUL_TOWER_BUTTON)
        {
            i->sprite.color = { 100,100,100,100 };
        }
        else if (grid_state[a] == Map::CHUL_TOWER_SETTED)
        {
            i->sprite.color = { 100,100,0,100 };
        }
        else if (grid_state[a] == Map::START_BUTTON)
        {
            i->sprite.color = { 0,0,100,100 };
        }
        else if (grid_state[a] == Map::START_BUTTON_SETTED)
        {
            i->sprite.color = { 0,0,100,100 };
        }
        else if (grid_state[a] == Map::END_BUTTON)
        {
            i->sprite.color = { 100,0,0,100 };
        }
        else if (grid_state[a] == Map::END_BUTTON_SETTED)
        {
            i->sprite.color = { 100,0,0,100 };
        }
        else if (grid_state[a] == Map::WAY_BUTTON)
        {
            i->sprite.color = { 0,100,100,100 };
        }
        else if (grid_state[a] == Map::WAY_BUTTON_SETTED)
        {
            i->sprite.color = { 0,100,100,100 };
        }
        else if (grid_state[a] == Map::START)
        {
            i->sprite.color = { 0,0,0,200 };
        }
        else if (grid_state[a] == Map::TOWER_POSITION_BUTTON)
        {
            i->sprite.color = { 50,20,30,200 };
        }
        else if (grid_state[a] == Map::TOWER_POSITION_BUTTON_SETTED)
        {
            i->sprite.color = { 50,20,30,200 };
        }
        else
        {
            i->sprite.color = { 255, 255, 255, 100 };
        }
        a++;
    }
}

void MAP_UPDATE_COMPONENT::Update(float dt)
{
    (dt);
    Check_Mouse_Pos();
}
