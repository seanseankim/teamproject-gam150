#pragma once
#include <Box2D/Common/b2Math.h>
#include <vector>
#include <engine/Object.h>
#include "engine\State.h"
#include "Component.h"


class Enemy : public Object
{
public:
    Enemy() = default;
    ~Enemy()
    {

    }
    void Hitted()
    {
        hp--;
    }
    int priority;
    int now_in = 0;
    int move_to = 0;
    bool is_reach_end = false;
private:
    std::vector<b2Vec2> enemy_path;
    float hitted_timer = 0.0f;
    int speed = 3;
    int hp = 5;
};
